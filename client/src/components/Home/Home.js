import React from "react";

import Bloc from "../Bloc/Bloc";
//import MyCardHeader from "../Carousel/Carousel"
import Header from "../Header/Header.js";
import Button from "react-bootstrap/Button"
import image from "./itts-logistiq.jpg";

import "./Home.css";

export class Home extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <div className="bg-img">
          <img className="img" src={image} alt="imagefw"/>
          <p className="text-white" >
          <h1>All The Way</h1>
            Nous mettons tout en œuvre pour satisfaire les exigences de nos
            clients, <br/> et nous repoussons les limites pour connecter et simplifier
            leur chaîne d’approvisionnement.
            <div className="button-group">
            <Button className="btn" variant="light">DEVENIR CLIENT</Button>
            <Button className="btn" variant="light">NOTRE SERVICES</Button>
            </div>
          </p>
          <div>
          <Bloc />
          </div>
          <br/>
          
          
        </div>
      </div>
    );
  }
}
