import React from 'react';

import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';

import { FaUserCircle } from 'react-icons/fa';
//import {FaGlobeEurope} from 'react-icons/fa';
import logo from "./logo.itts.jpg";

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import './Header.css';

class Header extends React.Component {
	render() {
		return (
			<div>				
			<Navbar className="header" bg="info" variant="dark" expand="lg">
				<Navbar.Brand href="/"><img className="logo" src={logo} alt="logo.itts"/></Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mr-auto">
						<Nav.Link href="#home">Home</Nav.Link>
						<Nav.Link href="#link">Link</Nav.Link>
						<NavDropdown title="Dropdown" id="basic-nav-dropdown" className="mega-menu">
							<NavDropdown.Header>
								<Container fluid className="container-xl">
									<Row>
										<Col>
											<Button variant="primary">Go somewhere</Button>
										</Col>
										<Col>
											<Button variant="primary">Go somewhere</Button>
										</Col>
									</Row>
								</Container>
								<Container fluid className="container-xl mt-4">
									<Row>
										<Col>Some quick example text to build on the card title and make up the bulk of the card's content.</Col>
										<Col>Some quick example text to build on the card title and make up the bulk of the card's content.</Col>
									</Row>
								</Container>
							</NavDropdown.Header>
							{/* <div className="overflow"></div> */}
						</NavDropdown>
					</Nav>
					<Form inline>
					    <Button variant="info"> <FaUserCircle className="icons"/> Se Connecter</Button>
						<FormControl type="text" placeholder="Search" className="mr-sm-2" />
						<Button variant="info">Recherche</Button>
					</Form>
				</Navbar.Collapse>
			</Navbar>
			</div>
		);
	}
}

export default Header;
