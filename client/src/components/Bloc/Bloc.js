import  React  from "react";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

import "./Bloc.css";


class Bloc extends React.Component {
  render() {
    return (
      <Container className="row m-auto items">
        <Card className="item">
          <Card.Body>
            <Card.Title className="Title">Suivre les expéditions </Card.Title>
            <input type="text" placeholder="Expédition, lettre de connaissement ou n° de conteneur"/>
            <Card.Text>
              <Button variant="outline-success">Traking</Button>
            </Card.Text>
          </Card.Body>
        </Card>
        <Card  className="item"> 
          <Card.Body>
            <Card.Title>Suivre les expéditions </Card.Title>
            <Card.Text>
              <Button variant="outline-success">Search</Button>
            </Card.Text>
          </Card.Body>
        </Card>
        <Card  className="item">
          <Card.Body>
            <Card.Title>Suivre les expéditions </Card.Title>
            <Card.Text>
              <Button variant="outline-success">Search</Button>
            </Card.Text>
          </Card.Body>
        </Card>
      </Container>
      
    );
  }
}

export default Bloc;
