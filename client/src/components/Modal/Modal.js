import React,{Component} from 'react';
import { Modal,ModalManager,Effect} from 'react-dynamic-modal';
//import { Button} from 'react-dynamic-modal';
 
class MyModal extends Component{
   render(){
      const { text,onRequestClose } = this.props;
      return (
         <Modal className="modal"
            onRequestClose={onRequestClose}
            effect={Effect.SuperScaled}>
            <p>{text}</p>
            <button onClick={ModalManager.close}>Trouver un prix</button>
            <button onClick={ModalManager.close}>Demande un devis</button>
         </Modal>
      );
   }
}

export default MyModal;