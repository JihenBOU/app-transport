import React from "react";

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import API from "../../utils/API";

export class Signup extends React.Component {
  state = {
    email: "",
    password: "",
    cpassword: ""
  };
  send = async () => {
    const { email, password, cpassword } = this.state;
    if (!email || email.length === 0) return;
    if (!password || password.length === 0 || password !== cpassword) return;
    try {
      const { data } = await API.signup({ email, password });
      localStorage.setItem("token", data.token);
      window.location = "/dashboard";
    } catch (error) {
      console.error(error);
    }
  };
  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };
  render() {
    const { email, password, cpassword } = this.state;
    return (
			<div className="Login">
				<Form>
					<Form.Group controlId="email" >
						<Form.Label>Email</Form.Label>
						<Form.Control autoFocus type="email" value={email} onChange={this.handleChange} placeholder="Enter email" />
						<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
					</Form.Group>

					<Form.Group controlId="password" >
						<Form.Label>Password</Form.Label>
						<Form.Control value={password} onChange={this.handleChange} type="password" placeholder="Password" />
					</Form.Group>

					<Form.Group controlId="cpassword" >
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control value={cpassword} onChange={this.handleChange} type="password" placeholder="Password" />
					</Form.Group>

					<Button onClick={this.send} block variant="primary" type="submit">
						Inscription
					</Button>
				</Form>
			</div>
		);
  }
}